﻿using uclock.ViewModel;

namespace uclock
{
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view models
            ////}
            ////else
            ////{
            ////    // Create run time view models
            ////}
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
            // Call ClearViewModelName() for each ViewModel.
        }



        #region MainViewModel

        private static MainViewModel _main;

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        public static MainViewModel MainStatic
        {
            get
            {
                if (_main == null)
                {
                    CreateMain();
                }

                return _main;
            }
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get { return MainStatic; }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Main property.
        /// </summary>
        public static void ClearMain()
        {
            _main.Cleanup();
            _main = null;
        }

        /// <summary>
        /// Provides a deterministic way to create the Main property.
        /// </summary>
        public static void CreateMain()
        {
            if (_main == null)
            {
                _main = new MainViewModel();
            }
        }

        #endregion



        #region MusicPlayerViewModel

        private static MusicPlayerViewModel _musicPlayerViewModel;

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        public static MusicPlayerViewModel MusicPlayerStat
        {
            get
            {
                if (_musicPlayerViewModel == null)
                {
                    MusicPlayer();
                }

                return _musicPlayerViewModel;
            }
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MusicPlayerViewModel MusicPlayerView
        {
            get { return MusicPlayerStat; }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Main property.
        /// </summary>
        public static void ClearMusicPlayer()
        {
            _musicPlayerViewModel.Cleanup();
            _musicPlayerViewModel = null;
        }

        /// <summary>
        /// Provides a deterministic way to create the Main property.
        /// </summary>
        public static void MusicPlayer()
        {
            if (_musicPlayerViewModel == null)
            {
                _musicPlayerViewModel = new MusicPlayerViewModel();
            }
        }

        #endregion


        #region ClockAddViewModel

        private static ClockAddViewModel _clockAddViewModel; 

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        public static ClockAddViewModel ClockAddStatic 
        {
            get
            {
                if (_clockAddViewModel == null)
                {
                    ClockAdd();
                }

                return _clockAddViewModel;
            }
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ClockAddViewModel ClockAddViewModel 
        {
            get { return ClockAddStatic; }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Main property.
        /// </summary>
        public static void ClearClockAdd() 
        {
            _clockAddViewModel.Cleanup();
            _clockAddViewModel = null;
        }

        /// <summary>
        /// Provides a deterministic way to create the Main property.
        /// </summary>
        public static void ClockAdd()
        {
            if (_clockAddViewModel == null)
            {
                _clockAddViewModel = new ClockAddViewModel(); 
            }
        }

        #endregion

    }
}
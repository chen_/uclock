﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using uclock.ClockClass;
using uclock.Model;

namespace uclock.ViewModel
{
    public class ClockAddViewModel : ViewModelBase
    {
        public ClockAddViewModel()
        {
            Init();
        }

        #region RaiseProperty


        private ClockRecord _dateTimeClock;

        public ClockRecord CurrentDateTimeClock
        {
            get { return _dateTimeClock; }
            set
            {
                if (_dateTimeClock != value) // 防止无必要的UI通知
                {
                    _dateTimeClock = value;
                    RaisePropertyChanged("CurrentDateTimeClock");
                }
            }
        }


        public ObservableCollection<ClockRecord> DateTimeClocks { get; set; }

        public ICommand AddRecord { get; set; }

        public ICommand DeleteRecord { get; set; }

        public ICommand TestBeer { get; set; }

        public ICommand TestEmail { get; set; }

        #endregion

        private void Init()
        {
            DateTimeClocks = new ObservableCollection<ClockRecord>(Respostory.GetAllClockRecord());

            TestBeer = new RelayCommand(() => BeepVulous.Beep(1000, 1000));
            TestEmail = new RelayCommand(() =>
                {
                    if (null != CurrentDateTimeClock)
                        EmailSend.Send(CurrentDateTimeClock.EmailAdress, "测试邮件");

                });

            AddRecord = new RelayCommand(() =>
                {
                    if (!DateTimeClocks.Contains(CurrentDateTimeClock))
                    {
                        DateTimeClocks.Insert(0, CurrentDateTimeClock);
                        Respostory.InsertRecore(CurrentDateTimeClock);
                    }
                }
                );
            DeleteRecord = new RelayCommand(() =>
                {
                    if (DateTimeClocks.Contains(CurrentDateTimeClock))
                    {
                        DateTimeClocks.Remove(CurrentDateTimeClock);
                        Respostory.DeleteRecore(CurrentDateTimeClock);
                    }
                }
                );
        }


    }
}

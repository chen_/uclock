﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using uclock.Model;

namespace uclock
{
    public class MusicPlayerViewModel : ViewModelBase
    {
        private static int _incuten = 0; 

        public MusicPlayerViewModel()
        {
            Init();
        }

        private void Init()
        {
            Musicses = new ObservableCollection<Musics>(Respostory.GetAllMusic());
            InsertMusic = new RelayCommand(() =>
            {
                if (!Musicses.Contains(SelectMusic))
                {
                    Musicses.Insert(0, SelectMusic);
                    Respostory.InsertMusic(SelectMusic);
                }
            });


            LastMusic=new RelayCommand(() =>
                {
                    _incuten = Musicses.IndexOf(SelectMusic);
                    if (-1 != _incuten)
                    {
                        SelectMusic = Musicses[_incuten - 1 >= 0 ? _incuten - 1 : 0];
                    }
                });

            NextMusic = new RelayCommand(() =>
                {
                    _incuten = Musicses.IndexOf(SelectMusic);
                    if (-1 != _incuten)
                    {
                        SelectMusic = Musicses[_incuten + 1 < Musicses.Count ? _incuten + 1 : Musicses.Count-1];
                    }
                });
        }

        #region RaiseProperty



        private double _sliderMax = 1;

        public double SliderMax
        {
            get { return _sliderMax; }
            set
            {
                if (Math.Abs(_sliderMax - value) > 0) // 防止无必要的UI通知
                {
                    _sliderMax = value;
                    RaisePropertyChanged("SliderMax");
                }
            }
        }



        private double _sliderCurrent;

        public double SliderCurrent
        {
            get { return _sliderCurrent; }
            set
            {
                if (Math.Abs(_sliderCurrent - value) > 0) // 防止无必要的UI通知
                {
                    _sliderCurrent = value;
                    RaisePropertyChanged("SliderCurrent");
                }
            }
        }



        private Musics _selectMusic;

        public Musics SelectMusic
        {
            get { return _selectMusic; }
            set
            {
          /*      if (_selectMusic != value) // 防止无必要的UI通知
                {*/
                    _selectMusic = value;
                    RaisePropertyChanged("SelectMusic");
               /* }*/
            }
        }



        public ObservableCollection<Musics> Musicses { get; set; }

        #endregion


        #region Icommadn

        public ICommand NextMusic { get; set; }
        public ICommand LastMusic { get; set; }
        public ICommand InsertMusic { get; set; }  
        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uclock.ClockClass
{
    internal static class ExrendMethod
    {
        public static string BoolCoverString(this bool orgrei)
        {
            return orgrei ? "1" : "0";
        }

        public static bool StringCoverBool(this string orgrei)
        {
            return orgrei == "1";
        }
    }
}

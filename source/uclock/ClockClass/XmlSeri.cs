﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace uclock.ClockClass
{

    public class CommXmlSerialize
    {




        public static void SaveSerializeXml<T>(T obj, String url)
        {
            File.WriteAllText(url, ObjectSerializeXml<T>(obj));
        }



        public static string ObjectSerializeXml<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var serializer = new XmlSerializer(typeof (T));
                serializer.Serialize(ms, obj);
                ms.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(ms, Encoding.UTF8))
                {
                    return reader.ReadToEnd();
                }
            }
        }


        public static T XmlDeserializeObject<T>(string xmlOfObject) where T : class
        {
            using (var ms = new MemoryStream())
            {
                using (var sr = new StreamWriter(ms, Encoding.UTF8))
                {
                    sr.Write(xmlOfObject);
                    sr.Flush();
                    ms.Seek(0, SeekOrigin.Begin);
                    var serializer = new XmlSerializer(typeof (T));
                    return serializer.Deserialize(ms) as T;
                }
            }
        }



        public static T ReadSerializeXml<T>(String url) where T : class
        {
            return XmlDeserializeObject<T>(File.ReadAllText(url));
        }


    }

}

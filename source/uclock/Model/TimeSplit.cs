﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uclock.Model
{
    public class TimeSplit
    {
        public char Y1 { get; set; }
        public char Y2 { get; set; }
        public char Y3 { get; set; }
        public char Y4 { get; set; }
        public char H1 { get; set; }
        public char H2 { get; set; }
        public char M1 { get; set; }
        public char M2 { get; set; }
        public char D1 { get; set; }
        public char D2 { get; set; }
        public char S1 { get; set; }
        public char S2 { get; set; }
        public char Mi1 { get; set; }
        public char Mi2 { get; set; }
        public char Her { get; set; }
        public char Piont { get; set; } 
    }
}

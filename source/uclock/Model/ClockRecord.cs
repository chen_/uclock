﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uclock.Model
{
    public class ClockRecord
    {
        public string Id { get; set; }
        public string RecordConent { get; set; }
        public string ClockDate { get; set; }
        public bool Music { get; set; }
        public string MusicName { get; set; } 
        public bool Beer { get; set; }
        public bool Email { get; set; }
        public string EmailAdress { get; set; }
    }
}

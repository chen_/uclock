# Windows 闹钟

### 简介
 Windows 闹钟是一款使用wpf做的桌面应用，可以定时提醒用户，UI界面时尚

### 功能列表
* 主界面显示当前时间，显示最近5个定时任务
* 左边菜单，可以添加自定义音乐，可用于闹钟音乐播放
* 右边菜单，可以添加定时任务，设置任务备注，蜂鸣题型、邮件题型、音乐题型
* 程序右键菜单，可以切换显示模式，切换成仙女下雪模式，指针分别为时、分、秒

### 技术说明
* .Net Framework 4.0
* SQLite
* MvvmLight
* Wpf

### 使用说明
* 直接运行 \uclock\source\uclock\bin\Debug  目录下的uclock.exe 文件即可

### 预览图
![](/ProductPreview/1.png) 
![](/ProductPreview/2.png) 
![](/ProductPreview/3.png) 
![](/ProductPreview/4.png) 
![](/ProductPreview/5.png) 

